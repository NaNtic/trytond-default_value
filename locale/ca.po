# 
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "error:default.value:"
msgid "The field %s has already a default value."
msgstr "El camp %s ja té un valor per defecte."

msgctxt "error:default.value:"
msgid "The field %s is a functional field."
msgstr "El camp %s es un camp funcional."

msgctxt "error:default.value:"
msgid ""
"You can not define more than one default value for the same model and field."
msgstr ""
"No podeu definir més d'un valor per defecte per al mateix model i camp."

msgctxt "field:default.value,boolean:"
msgid "Value"
msgstr "Valor"

msgctxt "field:default.value,char:"
msgid "Value"
msgstr "Valor"

msgctxt "field:default.value,create_date:"
msgid "Create Date"
msgstr "Data creació"

msgctxt "field:default.value,create_uid:"
msgid "Create User"
msgstr "Usuari creació"

msgctxt "field:default.value,date:"
msgid "Value"
msgstr "Valor"

msgctxt "field:default.value,datetime:"
msgid "Value"
msgstr "Valor"

msgctxt "field:default.value,default_value:"
msgid "Default Value"
msgstr "Valor per defecte"

msgctxt "field:default.value,field:"
msgid "Field"
msgstr "Camp"

msgctxt "field:default.value,field_type:"
msgid "Field Type"
msgstr "Tipus de camp"

msgctxt "field:default.value,float:"
msgid "Value"
msgstr "Valor"

msgctxt "field:default.value,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:default.value,integer:"
msgid "Value"
msgstr "Valor"

msgctxt "field:default.value,many2one:"
msgid "Value"
msgstr "Valor"

msgctxt "field:default.value,model:"
msgid "Model"
msgstr "Model"

msgctxt "field:default.value,numeric:"
msgid "Value"
msgstr "Valor"

msgctxt "field:default.value,rec_name:"
msgid "Name"
msgstr "Nom"

msgctxt "field:default.value,reference:"
msgid "Value"
msgstr "Valor"

msgctxt "field:default.value,selection:"
msgid "Value"
msgstr "Valor"

msgctxt "field:default.value,text:"
msgid "Value"
msgstr "Valor"

msgctxt "field:default.value,time:"
msgid "Value"
msgstr "Valor"

msgctxt "field:default.value,write_date:"
msgid "Write Date"
msgstr "Data modificació"

msgctxt "field:default.value,write_uid:"
msgid "Write User"
msgstr "Usuari modificació"

msgctxt "model:default.value,name:"
msgid "Default Value"
msgstr "Valor per defecte"

msgctxt "model:ir.action,name:action_default_value"
msgid "Default Values"
msgstr "Valors per defecte"

msgctxt "model:ir.ui.menu,name:default_value_menu"
msgid "Default Values"
msgstr "Valors per defecte"

msgctxt "view:default.value:"
msgid "Default Value"
msgstr "Valor per defecte"
